from django.contrib.contenttypes.models import ContentType
from django.template.loaders.filesystem import Loader as FilesystemLoader


class PhotoAlbumsLoader(FilesystemLoader):

    def __init__(self, engine, app_name, object_name):
        self.app_name = app_name
        self.object_name = object_name
        super().__init__(engine)

    def get_dirs(self):
        ctype = ContentType.objects.get_for_model(self.object_name)

        return [
            u"%s/%s/%s" % (self.app_name, ctype.app_label, ctype.model),
            u"%s/%s" % (self.app_name, ctype.app_label),
            u"%s" % self.app_name
        ]
