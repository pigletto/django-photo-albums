.. django-photo-albums documentation master file, created by
   sphinx-quickstart on Fri Sep 18 05:13:19 2009.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. toctree::
   :maxdepth: 2

==============================================
django-photo-albums's documentation
==============================================

django-photo-albums is a pluggable django image gallery app.

Image galleries can be attached to any Django model. And thanks to
django 1.1 url namespaces it is possible to have multiple 'albums' app
instances (for example, for different models) that use different sets of
templates, different permission rules, have dedicated integration test suites
and are available from different urls.

Each image gallery provide functionality for image viewing, editing,
uploading, uploading entire albums in one zip file, reordering,
marking/unmarking as main and deleting.

django-photo-albums is an application based on
`django-generic-images <http://bitbucket.org/kmike/django-generic-images/>`_ .
django-photo-albums requires Django >= 1.1 (or svn version with
url namespaces), setuptools for installation, django-annoying for some utils and
django-generic-images for image management and advanced admin image uploader.
django-generic-images and django-annoying will be installed automatically if
you install django-photo-albums via easy_install  or pip.

django-photo-albums does not provide any thumbnail creation solution because
there are external django apps (such as sorl-thumbnail) that would do this
better.

Testing if app instance is integrated correctly (at least that templates
don't raise exceptions) is easy because base class for integration testcases
is provided.


************
Installation
************
::

    $ pip install django-photo-albums

or::

    $ easy_install django-photo-albums

or::

    $ hg clone http://bitbucket.org/kmike/django-photo-albums/
    $ cd django-photo-albums
    $ python setup.py install

Then add 'photo_albums' and 'generic_images' to your ``INSTALLED_APPS`` in
settings.py and run ``./manage.py syncdb`` (syncdb is not needed if
django-generic-images was already installed).

Note: django-generic-images app provides admin image uploader (see more in
`django-generic-images docs <http://django-generic-images.googlecode.com/hg/docs/_build/html/index.html#admin>`_ ).
For this admin uploader to work ``generic_images`` folder from
``generic_images/media/`` should be copied to project's ``MEDIA_ROOT``.

Note: `django-composition <http://bitbucket.org/daevaorn/django-composition/>`_
is required if you want to use :class:`~generic_images.fields.ImageCountField`
or :class:`~generic_images.fields.UserImageCountField`. Run
``pip install django-composition`` to install django-composition.

****************************************************************
Why another photo albums app? Сomparison with pinax's photologue
****************************************************************

There is the one conceptual difference between django-photo-albums and
photologue: the data model.

Photologue data model
=====================

    ``Image <- (Many To Many) <- Gallery [ <- (ManyToMany, FK) <- Object ]``

or

    ``Image <- (Many to Many) <- Object``


django-photo-albums' data model (provided by django-generic-images)
===================================================================

    ``Image -> (GFK) -> Object``

Several galleries for one object can also be implemented by introducing custom
MyGallery model:

    ``Image -> (GFK) -> MyGallery -> (FK, GFK) -> Object``

This way images and galleries can be attached to any model and there is no need
to change model to attach images or albums to it.

Please note that there is small performance penalty for extra flexibility
provided by using generic foreign keys (1 extra query while selecting all
images for an object + 1 extra join with contenttypes table).


*********
Basic use
*********

PhotoAlbumSite
==============

.. automodule:: photo_albums.urls

    .. autoclass:: photo_albums.urls.PhotoAlbumSite()


Templates used by django-photo-albums
=====================================

Templates usually should be placed in ``templates/albums/<app_name>/`` folder.
App_name should be the name of queryset model's app as it appears in contenttypes
table (e.g. 'auth' for User). It is possible to override templates per-model
(by placing them in ``templates/albums/<app_name>/<model_name>/`` folder) or to
have a kind of default fallback templates for several apps (by placing them in
``templates/albums/`` folder).

Common context
--------------

Each view have at least 2 variables in context:

* ``<template_object_name>``: object for which gallery is attached to (the name
    of variable is set in PhotoAlbumsSite constructor
    (:ref:`here<template_object_name>`), default is ``'object'``)

* ``current_app``: app name, ``'albums'`` by default


Templates
---------

The views included in django-photo-albums make use of these 9 templates:


* ``show_album.html`` displays entire album

* ``edit_album.html`` displays entire album. Used by edit_album view.

* ``reorder_images.html`` displays entire album. Used by reorder_images view.
    These 3 templates have ``images`` variable in context with iterable of all
    images in gallery.

Example::

    {% for image in images %}
        <img src='{{ image.image }}' alt='{{image.caption}}'>
    {% endfor %}

With sorl-thumbnail::

    {% for image in images %}
        <img src='{% thumbnail image.image 100x50 %}' alt='{{ image.caption }}'>
    {% endfor %}


* ``show_image.html`` - displays one image. Has ``image``, ``prev`` and ``next``
    variables in context. ``prev`` and ``next`` are id's of previous and next
    (by image.order field) images in gallery.

* ``edit_image.html`` - displays one image for editing purposes. Has ``form``,
    ``image``, ``prev`` and ``next`` variables in context. ``prev`` and ``next``
    are id's of previous and next (by image.order field) images in gallery.
    ``form`` is a form of :ref:`edit_form_class<edit_form_class>` class.

Example::

    <img src='{{ image.image }}' alt='{{image.caption}}'>

    <a href='{% url user_images:edit_image album_user.id prev %}'>previous image</a>
    <a href='{% url user_images:edit_image album_user.id next %}'>next image</a>

    <form action='' method='POST'>
        {{ form }}
        <input type='submit' value='Save'>
    </form>


* ``upload_images.html`` - displays the formset for bulk image upload.
    Formset is of :ref:`upload_formset_class<upload_formset_class>` class and is
    available as ``formset`` context variable.

Example::

    <form action="" method="POST" enctype="multipart/form-data">
        {{ formset }}
        <input type="submit" value="Upload images">
    </form>


* ``upload_main_image.html`` - displays form for uploading one image. Uploaded
    image becomes main in gallery. Has ``form`` in context, it's a form of type
    :ref:`upload_form_class<upload_form_class>`.

* ``upload_zip.html`` - displays form for uploading zip archive with images.
    Has ``form`` in context, it's a form of type
    :ref:`upload_zip_form_class<upload_zip_form_class>`

* ``confirm_delete.html`` - displays confirmation dialog for deleting image.
    Has ``image`` in context. Should have a form that do POST request to delete
    view on submit.


Views
=====

.. automodule:: photo_albums.views
    :members:
    :undoc-members:

Forms
=====

.. automodule:: photo_albums.forms

    .. class:: photo_albums.forms.ImageEditForm()

        ::

            class ImageEditForm(forms.ModelForm):
                class Meta:
                    model = AttachedImage
                    fields = ['caption']


    .. _photoformset:

    .. class:: photo_albums.forms.PhotoFormSet

        ``modelformset_factory(AttachedImage, extra=3, fields = ['image', 'caption'])``

    .. autoclass:: photo_albums.forms.UploadZipForm()
        :members:

    .. autoclass:: photo_albums.forms.UploadZipAlbumForm
        :members:
        :show-inheritance:

    Example::

        if request.method == 'POST':
            form = UploadZipAlbumForm(request.user, obj, request.POST, request.FILES)
            if form.is_valid():
                form.process_zip_file()
                success_url = album_site.reverse('show_album', args=[object_id])
                return HttpResponseRedirect(success_url)
        else:
            form = UploadZipAlbumForm(request.user, obj)




*******
Testing
*******

Integration testing
===================

.. automodule:: photo_albums.test_utils
    :show-inheritance:
    :members:
    :undoc-members:

Bugs
====

Issue tracker is here: http://bitbucket.org/kmike/django-photo-albums/issues/

Bug reports, feature requests, enhancement suggestions are always welcome.
